CR_C := aarch64-linux-gnu-
MODULES := /home/user/raspberry/64/kernel-out/ # replace with your modules folder
obj-m := key.o

all:
	make ARCH=arm64 CROSS_COMPILE=$(CR_C) -C $(MODULES) M=$(shell pwd) modules
clean:
	make ARCH=arm64 CROSS_COMPILE=$(CR_C) -C $(MODULES) M=$(shell pwd) clean
