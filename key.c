#include <linux/module.h>
#include <linux/init.h>
#include <linux/tty.h>		/* For fg_console, MAX_NR_CONSOLES */
#include <linux/kd.h>		/* For KDSETLED */
#include <linux/vt.h>
#include <linux/vt_kern.h>
#include <linux/console_struct.h>	/* For vc_cons */
#include <linux/version.h>

MODULE_DESCRIPTION("Example module illustrating the use of Keyboard LEDs. - for kernel 5.4.68 ARM");
MODULE_AUTHOR("Kucio");
MODULE_LICENSE("GPL");

static struct myDevice {
    struct timer_list my_timer;
} myDevice;
struct tty_driver *my_driver;

#define BLINK_DELAY   HZ/5
#define ALL_LEDS_ON   0x07
#define RESTORE_LEDS  0xFF

/*
 * Function my_timer_func blinks the keyboard LEDs periodically by invoking
 * command KDSETLED of ioctl() on the keyboard driver. To learn more on virtual 
 * terminal ioctl operations, please see file:
 *     /usr/src/linux/drivers/char/vt_ioctl.c, function vt_ioctl().
 *
 * The argument to KDSETLED is alternatively set to 7 (thus causing the led 
 * mode to be set to LED_SHOW_IOCTL, and all the leds are lit) and to 0xFF
 * (any value above 7 switches back the led mode to LED_SHOW_FLAGS, thus
 * the LEDs reflect the actual keyboard status).  To learn more on this, 
 * please see file:
 *     /usr/src/linux/drivers/char/keyboard.c, function setledstate().
 * 
 */
static int val;
static void switch_key_fun(void)
{
    if(val == ALL_LEDS_ON)
    {
        printk("Restore");
        val = RESTORE_LEDS;
    }
    else
    {
        printk("Turn on");
        val = ALL_LEDS_ON;
    }
}

static void my_timer_func(struct timer_list * ptr)
{
    //Getting actual state of keyboard
    //Unusable in this example because it is always responding
    //with 0x122 no matter what was set in previous cycle
    //maybe it is a matter of my Microsoft Ergonomic keyboard...
    /*struct myDevice *driver_val;
    driver_val = from_timer(driver_val, ptr, my_timer);
    int* driver_addr = (int*)driver_val;
    printk("Status ptr: %d", *driver_addr);
	if (*driver_addr == ALL_LEDS_ON)
    {
        printk("Turn on");
		*driver_addr = RESTORE_LEDS;
    }
	else
    {
        printk("Turn off");
		*driver_addr = ALL_LEDS_ON;
    }*/
    switch_key_fun();
	(my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,
			    val);

	//myDevice.my_timer.expires = jiffies + BLINK_DELAY;
    myDevice.my_timer.expires = jiffies + 2*HZ; // 2 seconds
    add_timer(&myDevice.my_timer);
}

static int __init kbleds_init(void)
{
	int i;

	printk(KERN_INFO "Keyboard Led loading\n");
	printk(KERN_INFO "fgconsole is %x\n", fg_console);
	for (i = 0; i < MAX_NR_CONSOLES; i++) {
		if (!vc_cons[i].d)
			break;
		printk(KERN_INFO "poet_atkm: console[%i/%i] #%i, tty %lx\n", i,
		       MAX_NR_CONSOLES, vc_cons[i].d->vc_num,
		       (unsigned long)vc_cons[i].d->port.tty);
	}
	printk(KERN_INFO "finished scanning consoles\n");

	my_driver = vc_cons[fg_console].d->port.tty->driver;
	printk(KERN_INFO "tty driver magic %x\n", my_driver->magic);

	/*
	 * Set up the LED blink timer the first time
	 */
	timer_setup(&myDevice.my_timer, my_timer_func, 0);
	myDevice.my_timer.expires = jiffies + 2*HZ; // 2 seconds
    add_timer(&myDevice.my_timer);
	return 0;
}

static void __exit kbleds_cleanup(void)
{
	printk(KERN_INFO "Keyboard Led Unloading...\n");
	del_timer(&myDevice.my_timer);
	(my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,
			    RESTORE_LEDS);
}

module_init(kbleds_init);
module_exit(kbleds_cleanup);
